import React, { useEffect, useState } from 'react';
import './App.css';

const factory = require('./foo.js');

function App() {
  const [result, setResult] = useState(null);

  useEffect(() => {
    factory().then((instance) => {
      const fooResult = instance._foo(5);

      setResult(fooResult)

      // example calling `foo` function using `ccall`
      // ccall(ident, returnType, argTypes, args, opts)
      const otherFooResult = instance.ccall("foo", 'number', ['number'], [5]);
      console.log("foo(5) + 1 is:", otherFooResult);
    });
  }, []);

  return (
    <div className="App">
      <h1>WebAssembly in React</h1>
      <p>Result of `foo(5) + 1` is <b style={{ color: "red" }}>{result}</b></p>
    </div>
  );
}

export default App;
