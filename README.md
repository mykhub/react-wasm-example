# Simple WASM React example with C++/Emscripten

## Start application
- `npm i`
- `npm start`
- go to http://localhost:3000/


## Compiling C++ into JavaScript:
> This section explains how it works. You don't need to run it since `foo.js` is already precompiled, but keep in mind that when you change something in your `.cpp` file, you'll need to recompile it into a `.js` file using the command below.

```bash
# make sure you run this inside `src` folder, or in any folder where you have your `.cpp` file
emcc foo.cpp -o foo.js -sMODULARIZE -sSINGLE_FILE=1 -sEXPORTED_FUNCTIONS=_foo -sEXPORTED_RUNTIME_METHODS=ccall
```


### Command explanation

- `emcc`: The Emscripten compiler command. 
- `foo.cpp`: The source file containing the C++ code you want to compile.
- `-o foo.js`: Specifies the output file name. The compiled JavaScript code will be saved as `foo.js`.
- `-s MODULARIZE`: Wraps the generated JavaScript code in a module, which helps in better organizing the code and avoiding global namespace pollution.
- `-s SINGLE_FILE=1`: Generates a single JavaScript file containing all the necessary runtime code and compiled code. This makes it easier to include in a web application as you only need to include one file.
- `-s EXPORTED_FUNCTIONS='_foo'`: Specifies which functions from the C++ code should be exported and accessible from JavaScript. In this case, the foo function is exported.
- `-s EXPORTED_RUNTIME_METHODS='ccall'`: Specifies the runtime methods that should be exported and accessible from JavaScript. The ccall method is used to call C/C++ functions from JavaScript.

So, this command compiles `foo.cpp` to JavaScript, wraps it in a module, exports the `foo` function, and allows calling it from JavaScript using the ccall method. The output is saved as `foo.js`.


## Webpack configuration
To run compiled files properly webpack configuration was extended by using `react-app-rewired` library which overrides CRA webpack configuration without running `react-scripts eject` just by having `config-overries.js`.

```js
module.exports = function override(config, env) {
  config.resolve.fallback = {
    fs: false
  };
  return config;
};
```

## Eslint configuration
These rules were added to `.eslintrc` in order to be able to run compiled javascript files without errors
```json
{
  "rules": {
    "no-undef": "off",
    "no-restricted-globals": "off",
    "import/no-amd": "off"
  }
}
```

At some point you might want to put your generated files into separate folder and make specific set of rules for that directory

## Additional info
* [Documentation for installing `emscripten`](https://emscripten.org/docs/getting_started/downloads.html).
* [Emscripten Compiler options](https://emscripten.org/docs/tools_reference/emcc.html)
* [react-app-rewired](https://www.npmjs.com/package/react-app-rewired)
* [Guide for implementing C/C++ code in React using WebAssembly](https://dev.to/iprosk/cc-code-in-react-using-webassembly-7ka)

## Demo
![demo image](image.png)